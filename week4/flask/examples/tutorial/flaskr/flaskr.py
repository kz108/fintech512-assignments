from flask import Flask, request, render_template
import csv

app = Flask(__name__)

posts = [
    {
        "id": 1,
        "title": "Hello, World!",
        "body": "test",
        "author": "kz108"
            }
]

def get_post(post_id):
    for post in posts:
        if post["id"] == post_id:
            return post
    return None

@app.route("/")
def index():
    return render_template("index.html", posts=posts)

@app.route("/post/<int:post_id>")
def show_post(post_id):
    post = get_post(post_id)
    if post:
        return render_template('show_post.html', post=post)
    else:
        return "Post not found", 404

@app.route('/stock_history_graph')
def stock_history_graph():
    return render_template('stock_history_graph.html')

if __name__ == "__main__":
    app.run()
