import requests
from flask import Flask, request, render_template, jsonify
import json
import datetime


app = Flask(__name__)
app.config.from_pyfile('../../../../config.py',silent=True)
apikey = app.config["API_KEY"]


@app.route("/")
def index():
    return render_template("stock.html")


@app.route("/stock_info")
def stock_info():
    symbol = request.args.get("symbol")
    quoteUrl = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={apikey}'
    overviewUrl = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={apikey}'
    newsApiUrl = f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&apikey={apikey}'
   
    quoteResponse = requests.get(quoteUrl)
    overviewResponse = requests.get(overviewUrl)
    newsResponse = requests.get(newsApiUrl)

    quoteData = quoteResponse.json()['Global Quote']
    overviewData = overviewResponse.json()
    newsData = newsResponse.json()['feed']

    data_list = [quoteData, overviewData, newsData] 

    return json.dumps(data_list)


@app.route("/stock_ts")
def stock_ts():
    symbol = request.args.get("symbol")
    end_date = datetime.datetime.now().date()
    start_date = end_date - datetime.timedelta(days=365)
    timeSeriesUrl = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol={symbol}&apikey={apikey}&outputsize=full'
    timeSeriesResponse = requests.get(timeSeriesUrl)
    timeSeriesData = timeSeriesResponse.json()['Time Series (Daily)']
    data = {'dates': [], 'prices': []}
    for date in sorted(timeSeriesData.keys()):
        if start_date <= datetime.datetime.strptime(date, '%Y-%m-%d').date() <= end_date:
            data['dates'].append(date)
            data['prices'].append(timeSeriesData[date]['4. close'])
    return json.dumps(data)
