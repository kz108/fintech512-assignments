document.querySelector('#stock_btn').addEventListener('click', function(e) {
    e.preventDefault();
    var symbol = document.querySelector('#stock_symbol').value;
    show_StockInfo(symbol);
    
});


function show_StockInfo(symbol) {
    // Construct URL for stock info API with the given symbol
    var url = '/stock_info?symbol=' + encodeURIComponent(symbol);
    
    // Make an API call to fetch the stock info data
    fetch(url)
      // Parse the response as json
      .then(function(response) {
        return response.json();
      })
      // Pass the parsed data to the function
      .then(function(data_list) {
        show_stockinfo(data_list[0],data_list[1]);
        show_stocknews(data_list[2]);
      })
      
    // Construct URL for stock time series API with the given symbol
    var url_ts = '/stock_ts?symbol=' + encodeURIComponent(symbol);
    
    // Make an API call to fetch the stock time series data
    fetch(url_ts)
      // Parse the response as JSON
      .then(function(response) {
        return response.json();
      })
      // Pass the parsed data to the show_stockchart function
      .then(function(data) {
        show_stockchart(data);
      })
}


function show_stockinfo(data1, data2){
      const symbol = data2['Symbol'];
      const companyName = data2['Name'];
      const sector = data2['Sector'];
      const industry = data2['Industry'];
      const marketCap = data2['MarketCapitalization'];
      const peRatio = data2['PERatio'];
      const eps = data2['EPS'];
      const dividend = data2['DividendPerShare'];
      const dividendYield = data2['DividendYield'];
      const stockExchange = data2['Exchange'];
      const currentPrice = data1['05. price'];
      const prevClosePrice = data1['08. previous close'];
      const openPrice = data1['02. open'];
      const volume = data1['06. volume'];
      const highPrice = data1['03. high'];
      const lowPrice = data1['04. low'];
      const fiftyTwoWeekHigh = data2['52WeekHigh'];
      const fiftyTwoWeekLow = data2['52WeekLow'];

      // Update the page with the retrieved stock information
      document.querySelector('#stock_info').innerHTML = `
      <h2>${symbol} Stock Information</h2>
      <h5>Company Name: ${companyName}</h5>
      <h5>Sector: ${sector}</h5>
      <h5>Industry: ${industry}</h5>
      <h5>Market Capitalization: ${marketCap}</h5>

      <div style="display: flex;">

      <div style="flex: 40%; padding: 10px;">
      <p>Price to Earnings Ratio: ${peRatio}</p>
      <p>Earnings per share: ${eps}</p>
      <p>Dividend: ${dividend} (${dividendYield}%)</p>
      <p>Stock Exchange: ${stockExchange}</p>
      </div>

      <div style="flex: 40%; padding: 10px;">
      <p>Current Price: ${currentPrice}</p>
      <p>Previous Close: ${prevClosePrice}</p>
      <p>Open Price: ${openPrice}</p>
      <p>Volume: ${volume}</p>
      </div>

      <div style="flex: 40%; padding: 10px;">
      <p>High Price: ${highPrice}</p>
      <p>Low Price: ${lowPrice}</p>
      <p>52 Week High: ${fiftyTwoWeekHigh}</p>
      <p>52 Week Low: ${fiftyTwoWeekLow}</p>
      </div>

      </div>
      `;
}


function show_stocknews(data){
    const newsArticles = data.slice(0,5);
    let newsHtml = '';

      newsArticles.forEach(article => {
        newsHtml += `
          <div class="news-item">
            <a href="${article.url}" target="_blank" style="font-weight: bolder; text-decoration: none; color: blue;">${article.title}</a>
            <p style="border-bottom-width: 1px; border-bottom-style: solid;">${article.summary}</p>
          </div>
        `;
      });
      document.querySelector('#stock_info').innerHTML += newsHtml;
}


function show_stockchart(data) {
    var ctx = document.getElementById('stock_chart').getContext('2d');
    if (window.myChart) {
        window.myChart.destroy();
    }
    window.myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: data.dates,
            datasets: [{
                label: 'Close price',
                data: data.prices,
                fill: true,
                tension: 0.1,
                borderWidth: 1
            }]
        },
        options: {
            plugins:{
                title: {
                    display: true,
                    text: 'Price History',
                    font: {
                        size:36
                    }
                },
            },

        }
    });
}