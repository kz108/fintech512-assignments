from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)

from datetime import datetime
from flaskr.db import get_db
import requests

ALPHA_VANTAGE_API_KEY='4UJAPH4IUVTSLVOC'

bp = Blueprint('blog', __name__)

@bp.route('/')
def index():
    db = get_db()
    stocks = db.execute(
        'SELECT id, symbol, date_in, tracking_price, shares'
        ' FROM stocks'
    ).fetchall()

    rows = []

    for stock in stocks:
            symbol = stock['symbol']
            tracking_price = stock['tracking_price']
            shares = stock['shares']
            alpha_vantage_url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={ALPHA_VANTAGE_API_KEY}'
            response = requests.get(alpha_vantage_url).json()

            if 'Global Quote' in response:
                current_price = float(response['Global Quote']['05. price'])
                percent_change = (current_price - tracking_price) / tracking_price * 100
            else:
                current_price = 'N/A'
                percent_change = 'N/A'

            rows.append((symbol, tracking_price, shares, current_price, percent_change))

    return render_template('blog/index.html', stocks=rows)



@bp.route('/add_stock', methods=('GET', 'POST'))
def add_stock():
    if request.method == 'POST':
        symbol = request.form['symbol']
        tracking_price = request.form['tracking_price']
        shares = request.form['shares']
        error = None
        if not symbol:
            error = 'Symbol is required.'
        elif not tracking_price:
            error = 'Tracking price is required.'
        elif not shares:
            error = 'Shares is required.'
        else:
            # Add the stock to the database
            db = get_db()
            db.execute(
                'INSERT INTO stocks (symbol, date_in, tracking_price, shares) VALUES (?, ?, ?, ?)',
                (symbol, datetime.now(), tracking_price, shares)
            )
            db.commit()
            return redirect(url_for('index'))

        flash(error)

    return render_template('blog/add_stock.html')

@bp.route('/<int:id>/remove_stock',methods=('GET', 'POST'))
def remove_stock(id):
    db = get_db()
    db.execute('DELETE FROM stocks WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('index'))