import os
import requests
from flask import Flask, render_template, request, redirect, url_for, flash, g
from werkzeug.exceptions import abort
from flaskr import db
from flaskr import blog

ALPHA_VANTAGE_API_KEY='4UJAPH4IUVTSLVOC'

def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )
    
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
        
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    db.init_app(app)
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    return app
